package rule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import domain.CheckResult;
import domain.CheckRule;
import domain.Person;
import domain.RuleResult;

public class EMailRule implements CheckRule<Person>{
	private Pattern pattern;
	private Matcher matcher;
	
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public CheckResult checkRule(Person entity){
		//Pattern do e-maili znaleziony w internecie.
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(entity.getEmail());
		if(matcher.matches())
			return new CheckResult("", RuleResult.Ok);
		else
			return new CheckResult("",RuleResult.Error);
	}
}
