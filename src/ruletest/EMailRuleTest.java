package ruletest;
import org.junit.Assert;
import org.junit.Test;

import domain.CheckResult;
import domain.Person;
import domain.RuleResult;
import rule.EMailRule;

public class EMailRuleTest {

public EMailRule rule = new EMailRule();
	
	@Test
	public void checker_should_check_if_the_email_is_correct(){
		Person p = new Person();
		p.setEmail("costam@costam.pl");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok ,result.getResult());	
	}
	
}