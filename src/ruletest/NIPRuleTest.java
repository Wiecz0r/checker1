package ruletest;

import org.junit.Assert;
import org.junit.Test;

import domain.CheckResult;
import domain.Person;
import domain.RuleResult;
import rule.NIPRule;

public class NIPRuleTest {
NIPRule rule = new NIPRule();
	
	@Test
	public void checker_should_return_ok_if_nip_is_correct() {
		Person p = new Person();
		p.setNip("1234563218");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_nip_is_not_correct() {
		Person p = new Person();
		p.setNip("1234567890");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
}
