package cache;

import java.util.ArrayList;
import java.util.HashMap;


import cache.SystemEnumeration;

public class Cache{
	
	private Cache(){}
	
	private static Cache instance;
	private static Object token = new Object();
	private HashMap<String, ArrayList<SystemEnumeration>> cacheObjects = new HashMap<String, ArrayList<SystemEnumeration>>();
	private HashMap<String, ArrayList<SystemEnumeration>> cacheUpdate = new HashMap<String, ArrayList<SystemEnumeration>>();
	
	public static Cache getInstance(){
		if(instance==null){
			synchronized(token){
				if(instance==null)
					instance = new Cache();
			}
		    }
			return instance;
	}
	public void clear(){
		cacheUpdate.clear();
	}
	public void update(){
		setCacheObjects(cacheUpdate);
	}
	public HashMap<String, ArrayList<SystemEnumeration>> getCacheObjects() {
		return cacheObjects;
	}
	public void setCacheObjects(HashMap<String, ArrayList<SystemEnumeration>> cacheObjects) {
		this.cacheObjects = cacheObjects;
	}
}
