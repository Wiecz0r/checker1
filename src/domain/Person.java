package domain;

import java.util.Date;

public class Person extends Entity{
	private String firsName; 
	private String surname;
	private String pesel;
	private String nip;
	private String email;
	private Date dateOfBirth;
	
	//public Person (String firsName, String surname, String pesel, String nip, String email, Date dateOfBirth) {
		//this.firsName = firsName;
		//this.surname = surname;
		//this.pesel = pesel;
		//this.nip = nip;
		//this.email = email;
		//this.dateOfBirth = dateOfBirth;
	//}

	public String getFirsName() {
		return firsName;
	}

	public void setFirsName(String firsName) {
		this.firsName = firsName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	
}
