package domain;

public class PhoneNumber extends Entity{
	private String countryPrefix;
	private String cityPrefix;
	private String number;
	private String typeID;
	
	public PhoneNumber (String typeID, String number, String cityPrefix, String countryPrefix){
		this.countryPrefix = countryPrefix;
		this.cityPrefix = cityPrefix;
		this.number = number;
		this.typeID = typeID;
	}

	public String getCountryPrefix() {
		return countryPrefix;
	}

	public void setCountryPrefix(String countryPrefix) {
		this.countryPrefix = countryPrefix;
	}

	public String getCityPrefix() {
		return cityPrefix;
	}

	public void setCityPrefix(String cityPrefix) {
		this.cityPrefix = cityPrefix;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTypeID() {
		return typeID;
	}

	public void setTypeID(String typeID) {
		this.typeID = typeID;
	}
	
}
